
#include <iostream>
#include "Lista.h"

using namespace std;

int main(void){
    Lista *lista = new Lista();
    int cantidad;
    int n = 0;

    // El usuario define la cantidad de elemntos que tendra la lista
    // asi como los valores que agregara, mostrando los valores
    // en cada iteracion del ciclo For.
    cout<<"Ingrese la cantidad de elementos que desea: "<<endl;
    cin>>cantidad;
    for(int i=0; i<cantidad; i++){
        cout<<"Ingrese el numero que desea agregar: ";
        cin>>n;
        lista->nuevo(n);
        cout<<endl<< "Los valores actuales son: "<<endl;
        lista->imprimirLista();
        cout<<endl;
    }
    delete lista;
    return 0;
}