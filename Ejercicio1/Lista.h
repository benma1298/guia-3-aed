
#ifndef LISTA_H
#define LISTA_H
#include <iostream>

using namespace std;

// Se define la estructura Nodo.
typedef struct nodo{
    int dato = 0;
    struct nodo *prox;

} nodo;

class Lista{
    private:
    nodo *first = NULL;
    public:
    // Constructor de la clase.
    Lista();
    // Creacion de la Lista.
    void nuevo(int numero);
    // MUestra todos los datos de la Lista.
    void imprimirLista();
};

#endif