
#include <iostream>
#include "Lista.h"

using namespace std;

Lista::Lista(){
    nodo *first = NULL;
}

// Creacion de la Lista.
void Lista::nuevo(int numero){
    nodo *temp = new nodo;
    temp -> dato = numero;
    temp -> prox = NULL;

    if(this -> first == NULL){
        this -> first = temp;
    }else{
        nodo *now, *segundo;
        segundo = this -> first;
        while(segundo != NULL && segundo -> dato < numero){
            now = segundo;
            segundo = segundo -> prox;
        }
        if(segundo == this -> first){
            this -> first = temp;
            this -> first -> prox = segundo;
        }else{
            now -> prox = temp;
            temp -> prox = segundo;
        }
    }
}

// Funcion que imprime el contenido de las listas, usando un nodo
// temporal, para imprimir los valores de la lista mediante
// el comando While, el cual se repite hasta el espacio vacio de la lista.
void Lista::imprimirLista(){
    nodo *temp = this->first;
    while(temp != NULL){
        cout<<"["<< temp -> dato << "]";
        temp = temp -> prox;
    }
    cout<<endl;
}