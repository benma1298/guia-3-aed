
#ifndef LISTA_H
#define LISTA_H
#include <iostream>

using namespace std;

// Se define la estructura nodo.
typedef struct nodo{
    int dato = 0;
    struct nodo *prox;
} nodo;

class Lista{
    private:
    nodo *first = NULL;

    public:
    //Constructor
    Lista();

    //Crea la lista
    void nuevo(int numero);

    //Muestra los elementos de la lista
    void imprimirLista();

    //Une los elementos de ambas listas en una tercera lista
    void unirListas(Lista *lista);
};
#endif