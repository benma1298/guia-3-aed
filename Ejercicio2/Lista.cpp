
#include <iostream>
#include "Lista.h"

using namespace std;

Lista::Lista(){
    nodo *first = NULL;
}

// Funcion que crea la lista
void Lista::nuevo(int numero){
    nodo *temp = new nodo;
    temp -> dato = numero;
    temp -> prox = NULL;

    if(this -> first == NULL){
        this -> first = temp;
    }else{
        nodo *now, *segundo;
        segundo = this -> first;
        while(segundo != NULL && segundo -> dato < numero){
            now = segundo;
            segundo = segundo -> prox;
        }
        if(segundo == this -> first){
            this -> first = temp;
            this -> first -> prox = segundo;
        }else{
            now -> prox = temp;
            temp -> prox = segundo;
        }
    }
}

// Se recorre la lista usando la variable temp
// mientras que temp no sea igual a NULL.
void Lista::imprimirLista(){
    nodo *temp = this -> first;
    while(temp != NULL){
        cout<<"["<<temp -> dato<<"]";
        temp = temp -> prox;
    }
    cout<<endl;
}

// Se recorre la lista y se copia cada valor de esta
// en una nueva, denominada "lista3"
void Lista::unirListas(Lista *lista){
    nodo *temp = lista -> first;
    while(temp != NULL){
        nuevo(temp -> dato);
        temp = temp -> prox;
    }
}
