
#include <iostream>
#include "Lista.h"

using namespace std;


// La funcion ingresar se encarga de determinar la cantidad de numeros
// que tendra la lista, ademas de agregar el valor que se desea por
// cada iteracion del ciclo For.
void ingresar(Lista *lista){
    int cantidad;
    int n = 0;
    cout<<"Determine la cantidad de numeros que agregara: ";
    cin>>cantidad;
    cout<<endl;
    for(int i=0; i<cantidad; i++){
        cout<<"Ingrese el numero: ";
        cin>>n;
        lista -> nuevo(n);
    }
}

// Funcion main.
int main(void){
    // Se crean los objetos Lista.
    Lista *lista1 = new Lista();
    Lista *lista2 = new Lista();
    Lista *lista3 = new Lista();
    cout<<"-----------------------------"<<endl;
    cout<<"Ingresar datos a la lista 1: "<<endl;
    ingresar(lista1);
    cout<<"-----------------------------"<<endl;
    cout<<"Ingresar datos a la lista 2: "<<endl;
    ingresar(lista2);

    lista3 -> unirListas(lista1);
    lista3 -> unirListas(lista2);

    cout<<"------------------------------------------------------"<<endl;
    cout<<"Valores de listas 1 y 2, ademas de la union de ambas: "<<endl;
    cout<<"Lista 1: "<<endl;
    lista1 -> imprimirLista();

    cout<<"Lista 2: "<<endl;
    lista2 -> imprimirLista();

    cout<<"Lista 3: "<<endl;
    lista3 -> imprimirLista();

    // Se libera memoria del sistema.
    delete lista1;
    delete lista2;
    delete lista3;
    return 0;
}