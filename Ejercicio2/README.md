
Guia 3 Algoritmos y Estructura de datos
Ejercicio 2

Para compilar este programa, ejecutar make, y a continuacion ./Main2

En el presente ejercicio se pide generear dos listas enlasadas ordenadas, leyendo los datos que ingrese el usuario por la terminal, y que uniendo las dos listas creadas de numeros enteros, se crea una nueva lista con los valores de ambas listas.
Finalizando el programa, se muestran los datos de cada lista creada.