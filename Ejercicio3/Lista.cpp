
#include <iostream>
#include "Lista.h"

using namespace std;

Lista::Lista(){
    nodo *first = NULL;
    nodo *last = NULL;
}

// Funcion que crea la Lista
void Lista::nuevo(int numero){
    nodo *temp = new nodo;
    temp -> dato = numero;
    temp -> prox = NULL;

    if(this -> first == NULL){
        this -> first = temp;
    }else{
        nodo *now, *segundo;
        segundo = this -> first;
        while(segundo != NULL && segundo -> dato < numero){
            now = segundo;
            segundo = segundo -> prox;
        }
        if(segundo == this -> first){
            this -> first = temp;
            this -> first -> prox = segundo;
        }else{
            now -> prox = temp;
            temp -> prox = segundo;
        }
    }
}

// Se recorre la lista usando la variable temp
// mientras que temp no sea igual a NULL.
void Lista::imprimirLista(){
    nodo *temp = this -> first;
    while(temp != NULL){
        cout<<"["<<temp -> dato<<"]";
        temp = temp -> prox;
    }
    cout<<endl;
}

// Se crean nodos rec, que recorre la lista y sig, para saber el numero siguiente
void Lista::completarLista(){
    nodo *rec, *sig;
    rec = this -> first;

    while(rec -> prox != NULL){
        sig = rec;
        rec = rec -> prox;
        if((rec -> dato - 1) > sig -> dato){
            for(int i = sig -> dato + 1; i < rec -> dato; i++){
                nuevo(i);
            }
        }
    }
}
