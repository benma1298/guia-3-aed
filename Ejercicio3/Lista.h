
#ifndef LISTA_H
#define LISTA_H

#include <iostream>

using namespace std;

// Estructura Nodo
typedef struct nodo{
    int dato = 0;
    struct nodo *prox;
} nodo;

class Lista{
    private:
    nodo *first = NULL;
    nodo *last = NULL;

    public:
    // Constructor de la clase
    Lista();

    // Funcion que crea lista
    void nuevo(int numero);

    // Funcion que imprime la lista
    void imprimirLista();

    // Funcion que completa la lista y la imprime
    void completarLista();
};
#endif