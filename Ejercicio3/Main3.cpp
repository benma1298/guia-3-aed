
#include <iostream>
#include "Lista.h"

using namespace std;

int main(void){
    Lista *lista = new Lista();

    int cantidad;
    int n = 0;

    cout<<"Ingrese la cantidad de valores que desa agregar: "<<endl;
    cin>>cantidad;
    cout<<endl;

    for(int i = 0; i < cantidad; i++){
        cout<<"Ingrese el numero que desea agregar del menor al mayor: ";
        cin>>n;
        lista -> nuevo(n);
    }

    cout<<"Lista original: "<<endl;
    lista-> imprimirLista();

    cout<<"A continuacion, se rellenara la lista con datos faltantes:"<<endl;
    lista -> completarLista();

    cout<<"Lista original modificada: "<<endl;
    lista -> imprimirLista();

    // Se libera espacion en la memoria
    delete lista;

    return 0;
}