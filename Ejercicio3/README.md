
Guia 3 Algoritmos y Estructura de datos
Ejercicio 3

Para compilar el programa, ejecutar make, y a continuacion ingresar ./Main3

El presente ejercicio pide ingresar una serie de numeros enteros, que deben ser ordenados de menor a mayor, y cuando estos numeros sean valores no correlativos, el programa debe rellenar los numeros faltantes en la lista.
Finalizando el programa, se imprime la lista con los numeros ordenados de menor a mayor, con todos los numeros faltantes.
Por ejemplo:
Numeros ingresados: 1 - 4 - 7
Resultado final: 1 - 2 - 3 - 4 - 5 - 6 - 7
